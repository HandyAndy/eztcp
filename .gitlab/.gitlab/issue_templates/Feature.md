# Feature Request

## Enhancement Description
<!-- What is the issue being faced and needs addressing? !-->

## Proposed solution
<!-- How would you like to see this issue resolved? !-->

## Examples
<!-- Are there any examples of this which exist in other software? !-->

## Dependencies
<!-- Are there any other issues that this depends on? !-->

## Priority/Severity
<!-- Delete as appropriate. The priority and severity assigned may be different to this !-->
- [ ] High
- [ ] Medium
- [ ] Low
