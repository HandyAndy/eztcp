# EzTCP

Simple library to make TCP super easy for most use cases.  Takes care of queueing messages asynchronously, heartbeats and connetion healing, any-order connections, catching lost data, and handles the weird TCP errors that otherwise make LabVIEW TCP non-simple to use.


## Why EzTCP?

### Problem Statement

LabVIEW's TCP API works.  It is also based on a 20+ year old implementation of the winsock API and has some quirky behavior.  And even where it isn't quirky: distributed systems are hard, introduce more system states than we like to think about, and if you forget anything they tend to work fine for a while until they completely don't work fine.

 Here's a few things that you need to take into account if you're going to use LabVIEW's TCP API in a robust manner:

- You need to make sure your server is already listening when your client tries to connect.  Or you need to add code to your client to keep trying until the server is listening.
- You may want to send or receive messages in different parts of your application.  So you need to write a layer of interprocess communication to get things plumbed to where you need them.
- You need to create some sort of message format or protocol to tell the other side how to parse your messages.  And you need to address every edge case of how interpreting those values could go wrong.
- You need to handle error codes well.  That includes knowing which errors you can safely set aside and which ones mean you need to tear down your TCP connection.  
- If you only receive (and never try to send anything) from your end of a TCP connection, the API will not report some of those errors.
- If you need to know you are connected, you need to set up some sort of heartbeat exchange because TCP doesn't necessarily report every way in which a connection can die.
- You need a responsive application, so it is tempting to set timeouts to be quick - as is best pratice in most LabVIEW code.  But generic best practice doesn't apply here, and longer timeouts are far more robust for network operations.
- Your client may disconnect from your server, or become de-synchronized from it.  So you need to handle graceful discarding of the old connection and allow for a new connection, even if you only plan to have one machine talk to one machine.

And if you forget even one of these, your application may work fine for a quick test, but it will fail.  It might take a packet getting lost to trigger it, or a network delay, or a more rare LabVIEW error, or the other computer simply choosing to disconnect.  But it won't *reliably* work.

So stated differently, EzTCP has these features to make your life easier:

### Feature List

- Handles any-order connections
- Provides queueing mechanism to send or receive in different parts of your application
- Defines message protocol for the transport layer
- Handles errors, even quirky ones
- Sends heartbeat messages so you know if it's connected (and so some error cases get reported even if you only want to receive on your end of the TCP connection)
- Remains responsive to the application while also having long network-level TCP timeouts 
- Clients automatically attempt to reconnect.  Servers allow multiple connections and don't lock out reconnecting clients

## Why not EzTCP?

Each tool is good for some things but bad for others.  Why might you not want to use EzTCP?  

- If you need to have a server run separate TCP connections with separate behavior on one port.  EzTCP treats all clients as the same - the kind of simplification that makes things really quick and easy to use.
- If you need to write a TCP library to talk to a piece of 3rd party equipment - an instrument, a PLC, or odd protocol.  EzTCP already has a defined wire protocol, which is going to be different from what those devices require.  I've thought about allowing that behavior to be overridden, but that wouldn't be a great solution.  I'm thinking we distill the lessons learned in the code into a sort of TCP Toolbox, which you could use in this scenario.  Until then, feel free to learn what you can from what is here.
- If you need this library to do application-level acknowledgement for you.  In other words, EzTCP is not for you if either:

    - Your application cares about data not getting lost and you want the library to solve your problem.
    - You are willing to write code at your level to try again if data doesn't make it, but want the library to do most of the work.

    EzTCP might be good for you if:

    - Your application, if it encounters lost data or drops a TCP connection, will just shrug and keep on ticking - trying to reconnect and continue sending what hasn't been sent.
    - Your application depends on critical data getting across the wire, but is willing to do some double checking to make sure that the receiver got it all (replying to the sender after a successful transfer, for instance)
   
    I would like to add application-level acknoledgement at some point - but this has very much resulted from evolutionary, incremental changes of trying to make TCP code a little better.

## How do I use it?

Head over to the [How to Use EzTCP.md](./How%20to%20Use%20EzTCP.md) file to find a walkthrough of the examples.

## How does it work?

Head over to the [Theory of Operation.md](./Theory%20of%20Operation.md) file to find an explanation of how it works.

## LabVIEW Version

Built in LV 2019.  Usable in 2019 and up.  Unless you want to save for earlier, in which case it will likely work as far back as you wish to take it.  Keep in mind it does include a class.

## Quality, Limitations

Built on lessons learned from many projects.  Headaches and tears have been translated into code that makes things better.  It may not be perfect yet, but it sure is better than any new-from-scratch network code that just about anyone would write.  If you don't want to use this library, please reach for another library before you go build your own.

As it stands today, it is mainly intended for communications with LabVIEW on both ends - but it could be adapted for use with other languages. 

## Dependencies

- EzQ v0.1 (included in repo) from [https://gitlab.com/HandyAndy/ezq](https://gitlab.com/HandyAndy/ezq) - this is mostly a set of typedefs that let you decouple your application from EzTCP directly.
- Exitimer from Endigit (included in repo for now) - this is a library that combines a timer and an exit message to time and stop loops
- Error Accumulator (included in repo for now) - This collects logs and puts them in a log file
- LogRotate from NI (dependency of Error Accumulator (included in repo for now) - Used by Error Accumulator, this rotates logs on disk

## License

MIT License - see license file for full details