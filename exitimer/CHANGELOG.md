## 0.1.0
Initial release.

## 1.0.0
Locking the API.

Addressing bug when Create if Null.vi is called after Exit.vi is called. Previously this would recreate the Exitimer, the fix prevents re-creation.

Moving examples to a separate project.