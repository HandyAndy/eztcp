# Exitimer

Exitimer is a LabVIEW object that provides an easy reference-based way to time and shut down loops.

## Getting Started

We recommend using G Package Manager (GPM) to install Exitimer. GPM installs the package next to the rest of your project source code and makes it easy to reuse on all your projects.

## LabVIEW Compatibility

Works with LabVIEW 2013 and later. Active development is in LabVIEW 2017.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/endigit/exitimer/tags). 

## Authors

* **Marty Vowles** - *Initial work* - [Endigit](http://www.endigit.com)
* **Shawn Walpole** - *Bugfixes* [Endigit](http://www.endigit.com)
* **Andrew Heim** - *Examples* [Endigit](http://www.endigit.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Shawn Walpole for the idea that kicked this off.