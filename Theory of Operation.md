
# Theory of Operation

Here is some detail about how EzTCP works, why, and how to best use it.  Remember the wise words of the meme maker: "Just 6 hours of debugging can save you 5 minutes of reading documentation".

The goal for this documentation is to help a future person in my shoes who needs to understand this library to use it to the fullest of its abilities, or to modify it to fix a problem or meet a different use case.

If you just want the quickest route to deploy some hands-off TCP code, just go run it.  There's a couple of examples. In fact, you probably haven't even read this far.  If that is you and you have read this far, congratulations... and I'd love to know about it.

There are few assumptions scattered throughout, like this one:

**Assumption** EzTCP was built primarily for embedded control, test, or data acquisition applications.

# Connections and re-connections

I've often seen TCP used like this, with one connection on two ports, each sending data one way:

![Two one-way connections](./Diagrams/One-way_connections.png "Two one-way connections")

It has a few problems.  One is that the LabVIEW API doesn't return some errors if you never attempt to write to it.  Another is that this bidirectional scheme could be accomplished with only one connection.  That is, if it were appropriate.

So the next level of improvement is to use a connection bidirectionally:

![Two-way connection](./Diagrams/Two-way_connection.png "Two-way connection")

But if done without asynchronously launched loops, simple server code might not release quickly enough when a client disconnects.  In that case, the server continues using the connection it had, while the actual, reconnection client can't reach it.

![Tough to reconnect](./Diagrams/Tough_to_reconnect.png "Tough to reconnect")

So for EzTCP, we made a simplifying assumption here.  
**Assumption**  We can treat all incoming messages on this EzTCP session as coming from the same remote host.  And that all messages sent get sent to all remote hosts.  

That lets a server treat all clients symmetrically, and accept new clients or copies of the same client:

![Multiple connections](./Diagrams/Multiple_connections.png "Multiple connections")

But that assumption gets rid of one of the key features of TCP - that of a server being able to establish different and separate connections on a single port.  EzTCP treats all connections on a given port as being more or less indistinguishable.  Not a bad simplification for many small projects, but a tradeoff to keep in mind for applications with more complex networking requirements.  

We should also note that an EzTCP client will continue attemtping to connect or re-connect as long as its EzTCP session is open.  An EzTCP server will always attempt to listen for new connections as long as it is open, too.  That means if we have a use case where we need to tell it to stop, we should send a message over the session to control that higher-level logic.  At least for now.

EzTCP (at least at this point) does not automatically tear down the EzTCP session.  It does create, listen, connect, disconnect, and otherwise manage TCP connections underneath itself.  Note the terminology here - TCP has a *connection*, but EzTCP calls itself a *session*.  Like described in the OSI model, it manages TCP connections more or less transparently to the code that calls it.  A session starts when that code calls Create Client or Create Server, and doesn't end until Close TCP Messenger is called.

As a final note, EzTCP uses the lower level VIs to manage a TCP Listener.  That is because the higher-level all-in-one TCP Listen calls the "Internecine Avoider".  This VI is there to prevent you from opening multiple listeners on one port.  You'd think the operating system would report that error in the API but it does not.  However, the Internecine Avoider also is prone to major performance problems in certain circumstances - totally preventing new connections for minutes in certain RT scenarios.  There is an issue for the issue where we could have a collision of listeners and not know about it.

# Message queueing

Once a TCP connection is established, a set of loops is launched to handle the message and wire protocol (see the next section).  To get messages to and from the wire protocol VI, some queues get created.

This section details the queues internal to EzTCP.  Before diving into that, realize that queues exist on the sending and receving side.  And being queues, they can fill up if they are being emptied slower than they are being filled.  For instance, sending periodic messages before you are connected to anything will cause the data to get buffered.  That is a good thing, until it exhausts your memory and everything crashes.

I have thus far resisted exposing API operations such as Flush Queue, because most of the reasons I can think of for using those are better off avoiding in the first place.

### Receive queue

A single receive queue is created when Create Client or Create Server is called.  All traffic received is sent over it to the application.  It gets read when Receive TCP Message is called.  

That means most likely all reading should be done in one place in the code.  For situations where messages need to be sent to different locations in code, see EzQ.  It is a minimal library meant to help break dependencies in these cases.

### Primary send queue

A primary send queue is created when the EzTCP session is created.  For a client, that is all that is needed, since only one TCP connection will be up at a time.

A single server can potentially connect to multiple clients at once.  Since each of those TCP connections can pass data at different rates, server needs a separate queue for each.  To copy the sent messages into each of these, the server runs a dispatcher loop.  There it copies messages from the primary send queue to per-connection send queues.

### Per-connection send queues

These only exist in a server session.  A queue is created each time a TCP connection is successfully established.  Any new messages that come into the primary send queue are copied into any per-connection send queues that are active, to accomodate potential differences between TCP connections.

# Message definition

In this API, a message is two strings - a name and data text.  While both are technically optional, it is best practice to use both.

## Parsing messages

TCP is a byte stream, or a protocol that tries really hard to make all the bytes that were sent appear as one seamless, endless supply.  So to make it useful in most cases, you need a way to divide it into messages.

One way, common since the 70s and RS-232, is using text and termination characters.  A character, for instance the end-of-line character, is chosen as the end of each message.  The receiver scans until it sees that character, and that is interpreted as one message.  That has the advantage of being simple, but only until you need to send aribrary data.  Then you need to make sure your data is clean of such symbols and transform them.  In http, Base64 encoding is used to turn arbitrary data into http-safe text for these reasons.  It costs extra space though, because it maps the data into a subset of the available byte values.  And data must be transformed and re-transformed at each end.

Another option is fixed-length messages.  While some protocols like CAN use it well, the entire protocol stack is built that way so it can at least make use of the tradeoffs.  To force that on a byte stream such as TCP is both cumbersome to write and gives no benefits in trade.

The last one (I know of, anyways) is to precede each message with the message size.  This allows you to take advantage of variable-length messages.  It also can be done with relatively simple code.

## Best practice for message parsing

### Message Name

It is common for LabVIEW developers to want to directly pass class objects (children of a "message" class) or use enums as message identifiers.  Enums cause the code in the message parsing library to not be reusable.  Objects are often used with introspection, which is abominably slow in LabVIEW.

Instead, use a string!  EzTCP isn't coupled to any application- or message-specific enums or code.  If passing classes, use the name of the message class if nothing else - then the receiver can select their case without needing to use introspection.  (And if you really must pass multiple classes over one message, please just write a "name" method that gets overridden by each child - it's sadly much faster than using LabVIEW's Get LV Class Name.vi)

That is why EzTCP is based around string names.

How do I avoid typos in my code (one great reason for using enums)?  Use some sort of constant, like a VI constant or a global-defined constant.

### Message Data

After following best practice for the Name field, you can take any needed data that accompanies that message and flatten it to a string.  This may mean that you have a cluster for more complicated messages.  It may mean a class object.  But it can also be a single data element.

The important thing to remember is that the Name exists to identify the message and case off of.  So, the data field can be specific to that message - and does not need to be self-identifying.

# Wire protocol

The wire protocol is the data that the two sides exchange, in terms of what goes over the wire (not how the code is written).

Here's a simple overview - for exhaustive detail... see the code.

One thing to note is that the Wire Protocol is perfectly symmetrical.  That's right - all the assymetry of client and server behavior is around setting up the connection.  Once a connection is made, both sides run the same Wire Protocol VI.  That makes it simple to understand as well as better to debug.  And more like the TCP layer below it, too.

And in case you missed it, the entire wire protocol is defined (for both sides) in the Wire Protocol VI.

## Message size

The message starts with an i32 that is the length of the message.  Why an i32 and not a u32?  Because LabVIEW uses that datatype in those kinds of fields and we do it to avoid unnecessary conversions.

After the overall size, the message follows:

- Size of Name (i32)
- Name string
- Size of Data (i32)
- Data string

There are no delimiters - the message is constructed by looking at the size of the string, and then parsed using those same sizes.  So if you sent "message420" as a message, there is no danger of EzTCP getting confused about where the message stops and where the size begins.

Yes, it's slightly redundant to include an overall message size as well as the size of both parts.  But as long as we throw away bad messages at each step, we don't get ourselves into a Heartbleed situation.

Message size also presents an interesting conundrum if you receive an absurdly high message size, because then the code will attempt to read all if it in one go - needing contiguous memory I might add.  That is likely to lead to problems.  It is for that reason that there is a maximum message size.  

EzTCP will discard a message with a larger size than its maximum.  And if you're paying attention, it will report an error if you try to send it as well (and it won't actually get sent).  Don't send really big things in one message - break it up into pieces.  A message is atomic, at least in the LabVIEW code.  Sure, TCP breaks it up under the hood, but it also breaks things into 1500 byte pieces.  Messages over a few MB in size will likely affect your system, depending of course on your memory constraints.  By means of best pratice, please at least consider breaking up large messages before trying to increase the message size.

If breaking it up is impractical, increase the maximum message size on the EzTCP Create call.  Don't forget to do it on both sides.

## Heartbeats

Heartbeats are sent on a pre-determined interval, but only if some other message hasn't been sent in that long.  Heartbeats are invisible to the caller of EzTCP, exchanged across the TCP connection from Wire Protocol VI to Wire Protocol VI but not exposed directly as messages.

Heartbeats are simply a message with a size of -1 and no further data (the other 4 fields do not follow in this case).  That's right - an i32 with a value of -1.  

Receiving either a data message or a heartbeat resets counters and such on both sides preserving the connection status.  Or they are the basis for concluding that the connection has gone dead when no message has been received after the prescribed time.  That value is specified in the Wire Protocol VI.

## Odd error cases (mostly deprectated)

There were a few situations where EzTCP sent a message to the other side of the connection for the purpose of communicating an error.  For that it used a message with a size of -2 (and no further data).  That has been largely deprecated, but some code remains for now.  These messages were invisible to the caller as messages, but they did cause the TCP connection to get shut down and restarted.

These existed for telling the other side to shut down if it sent too large a message, or if a de-sync was detected.  The first case has been addressed by refusing to send messages that are too big.  The second has been addressed by managing timeouts better - see that section below.

## Errors

If you spend time with the LV TCP API, you learn it has a personality.  The biggest of which is the need to send in order to have it report that the other side has aborted the connection!

The code has a catalogue of several errors and whether they signal the need to tear down the connection or not.  This was earned with time and tears - because the descriptions alone do not communicate much subtlety.

I plan to catalog these errors in text in this repo, for the sake of posterity.

## How TCP timeouts are managed

The LabVIEW community has adopted the standard practice of making timeouts short on most functions in most places.  This keeps code simple and allows applications to remain responsive.  This is also not the way to handle TCP calls.

TCP involves sending data and sometimes re-sending data.  If a packet gets dropped and you have too short a TCP Read time, it can't be successfully resent in that window.  Then you have a timeout error, a fragmented message, and your two sides are out of sync.  Being out of sync means that you don't know where the start of the next message is, and it's better to tear down the TCP connection and restart it.

Long timeouts means the application may take 30 seconds to stop.  That's not great either, but marginally better than collapsing whenever any packets are dropped.

The answer to this is call TCP Close in parallel with the other calls.  When that gets called, any TCP Read or Write calls still waiting will instantly return so you can shut down your problem.  By doing that, you gain the benefit of having practically infinite timeouts and also have a perfectly responsive application.

There is one corner case not addresed by that however, and that is the case where many connections are being made and torn down.  Some embedded OSs are sensitive to mismanagement of their finite resources.  If the server is the one initiating the TCP Close, it keeps its local TCP socket set to a TIME_WAIT state for several minutes. Read something like [Wikipedia's TCP article](https://en.wikipedia.org/wiki/Transmission_Control_Protocol) or [RFC 9293](https://www.ietf.org/rfc/rfc9293.html) if you want to know why (it's part of the TCP spec_).  Some embedded systems (VXWorks cRIOs were sensitive to this) can lock up once a certain number of sockets are in that state.  Embedded Linux seems to be more robust against that, but I'm documenting it here to pass on the clue.  If you have a server making and breaking many connections, it is a good practice to have the client be the one to call TCP Close. (Assuming the client makes fewer connections. If not, why can't you just keep using them?)

# EzTCP and detecting data loss
This gets a bit nuanced.  But is important where data loss MUST be avoided.

## TCP prevents data loss
This section title is absolutely a falsehood.  Yet is is repeated in many places where people learn about TCP.  TCP is not vastly different from UDP that does suffer data loss - it simply tries hard to catch up when it does.  Yeah, it re-sends data that isn't acknowledged in a certain time.  But it can't make the impossible happen.

If I cut the ethernet cable, TCP isn't going to save you.  So then what does TCP offer with regards to data loss?

## TCP detects data loss
TCP's feature is that it *detects* data loss.  Yes it tries to not lose it, within its limited ability.  But when it can't get that data there, it will tell you.

If you're writing, this comes as a timeout on the Write function.  Or in the case of an infinite timeout, many seconds or minutes passing and that function still not returning.  If you're reading, it's the same.  Ok so we've detected the error.  Which bytes of data got through and which ones didn't?

That is a diabolically hard problem to answer.  TCP can't tell you because it has buffers on both ends of the connection (including Nagle buffers that we can disable, and others we can't).  And even if (on the sending side) it could tell you the last byte that got acknowledged, it's possible there are others that made it that weren't acknowleged.  And there is buffering between the TCP process and the application where it might have gotten lost,too.  If you think too much about it, this problem quickly descends into a particular kind of madness dubbed Byzantine Fault Tolerance.  In summary, for two computers that aren't exchanging messages anymore, you simply can't know the last thing they received.

So TCP can detect data loss, but can't tell us what the last data was to make it through successfully.  

## How EzTCP deals with TCP and data loss

EzTCP can tear down  and reconnect TCP connections when it wants to.  Each time it does so, there's a potential data was lost - specifically at the end of the TCP connection.

Making the TCP timeouts longer, or even "infinite" (or several minutes in practice) give you a good shot of preventing that.  Note that TCP connections really are just state tables that exist on the computers at both ends.  If you were to inspect your system with something like netstat you would likely have many connectinos that have been up for hours or days.  In most cases with the now-default infinite timeout, EzTCP will keep connections up indefinitely unless your code closes them.  

But when a TCP connection dies, EzTCP doesn't notify you.  Why?  Because it instead tries to reconnect. And because with an infinte timeout, we can assume that as long as the TCP connection can continue (even if it slows way down or gets intermittent for a while), there is no data loss.  If it comes time to tear it down, we know about it, and any chance of data loss is only at the end.  If we are calling TCP Close after normal operation, we have data we need - so the only case we need to worry about is that of an error.  

So EzTCP could add something like a "paranoid mode" that notified you if the underlying TCP connection had a problem and there was a chance of data loss.   But there is a better way - because even  "paranoid mode" would only really tell us the message got to the TCP socket on the other machine.  It could have gotten lost in buffering between there and the application on the other side.

## Application Level Acknowledgement 

What do you do when you really can't handle one byte being lost?  The real answer is called application-level acknowledgement.  The sender keeps the data in its entirety until it is acknowledged by the remote *application* that the remote *application* received it in full.  The logic needs to extend past the network layer into the application - or there would be a gap in some buffer or queue between the layers where it could still get lost after being acknowledged.  So the logic needs to be worked into the application itself.  

Adding application level acknowldgement demands more of the application.  It has to handle the case where the network failed.

--*this section under construction*--

(...explanation of one message outstanding and why that's the dominant approach.  Multiple outstanding requires lots of application involvement.)


As far as transferring a high speed stream without any loss (or a detectable failure), it also needs application-level acknowledgement.  That takes extra overhead and costs performance.  LabVIEW's Network Streams can do this, but it takes a Flush call after each message, which takes a round trip between the two machines and slows things down.

EzTCP does not implement application-level acknowledgement at this time.  Note that to do so would require one of two structures:

1. One message is outstanding at a time.  That way the application logic that would respond to a lost message can have a clear and straightforward way to respond.  But this can in some cases severely impact performance, since it interferes with buffering and taking advantage of the full bandwidth of the connection.
2. In order to take full advantage of buffering but also maintain individual acknowledgements for each message, it would take a complex setup of asynchronous read receipts.  Imagine a setup where for each message that gets sent, the application keeps a copy (perhaps a map in memory).  When the remote side acknowledges each message, they get removed from the map.  If there is data loss, the map will contain the outstanding un-acknowledged data.  I think this one would be neat to see, but would be very complex internally and require complex interactions from the application calling it.  Alternatively, it might be able to be added as another layer on top of EzTCP.

## In summary
EzTCP does not implement application-level acknowledgement, so there are a few cases in which data can be lost - and undetected, if you're not watching.  What's different from TCP here is that such loss might be in the middle of one EzTCP session (between two TCP connections) instead of just at the end of a TCP connection (if you were to use bare TCP).  So it may drop data from what appears like the middle instead of the end. 

You can add a simple application level acknowledgement on top of EzTCP if you want (sending "ok" in reply to each message sent, for instance).  And if you stick with the default timeouts (and just one use TCP connection), any loss should be limited to being very rare, such as after long network disruptions.  It has been tested with up to 50% packet loss and is able to work well - but complete network disruption eventually seems to lead to one of the operating systems dropping the connection.

# Things that still bug me about EzTCP

## Ports and connections

EzTCP makes a server blind to the differences between all clients on a given port.  Making it aware of these differences will significantly change the API experience.  Some applications need it though, so I'm considering building it on as another mode.

## Data loss

We built a library on top of TCP and didn't add application-level acknowledgement.  So there is one big remaining flaw with it.  That makes me want to add it in the future.  

## Non-Labview compatibiility

Having a simple, well-defined wire protocol makes it possible to implement in other languages.  I've done it before.  But there needs to be at least a certain subset of the library there for it to work.

The major bummer is not being able to base other TCP libraries off of this - the kind where you talk to a 3rd party instrument or industrial device.  That PLC isn't going to know what to do with your packet that is just a "-1".  

I have a dream of extracting parts of this libary (errors and how they are handled, the best ways of establishing a connection, best practice for timeouts and other such things) and making a TCP Toolbox that could be used for building TCP-based libraries that need to talk in a more constrained way.  Until then, feel free to use this repo as a reference.

## Lack of message routing - mostly solved

Compared to other messaging libraries (NI's AMC, ZeroMQ, MQTT, and others), routing between processes in your program always seems to be at least partially in scope.  With EzTCP it wasn't, until we figured out EzQ.  

Now with EzQ you can pass messages specifically to one module, without having to modify all the code in between.  And send messages.  And develop modules that couple to EzQ, but not to all of EzTCP.

EzQ is still being used in its first project so is likely to evolve, but seems to be a step in the right direction.