﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Examples" Type="Folder">
			<Item Name="Simple multi-client example" Type="Folder">
				<Item Name="Multi-client server.vi" Type="VI" URL="../Source/examples/Multi-client server.vi"/>
				<Item Name="German client.vi" Type="VI" URL="../Source/examples/German client.vi"/>
				<Item Name="Brazilian client.vi" Type="VI" URL="../Source/examples/Brazilian client.vi"/>
				<Item Name="Australian client.vi" Type="VI" URL="../Source/examples/Australian client.vi"/>
			</Item>
			<Item Name="Round-trip timing" Type="Folder">
				<Item Name="Timing test client.vi" Type="VI" URL="../Source/examples/Timing test client.vi"/>
				<Item Name="Timing Test server.vi" Type="VI" URL="../Source/examples/Timing Test server.vi"/>
			</Item>
			<Item Name="Variable-size messages" Type="Folder">
				<Item Name="Variable-size Server.vi" Type="VI" URL="../Source/examples/Variable-size Server.vi"/>
				<Item Name="Variable-size Client.vi" Type="VI" URL="../Source/examples/Variable-size Client.vi"/>
			</Item>
			<Item Name="Simple client example.vi" Type="VI" URL="../Source/examples/Simple client example.vi"/>
			<Item Name="Simple server example.vi" Type="VI" URL="../Source/examples/Simple server example.vi"/>
		</Item>
		<Item Name="Tests" Type="Folder">
			<Item Name="Data loss client.vi" Type="VI" URL="../Source/tests/Data loss client.vi"/>
			<Item Name="Data loss server.vi" Type="VI" URL="../Source/tests/Data loss server.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Folder">
			<Item Name="Nagle SubVIs" Type="Folder">
				<Item Name="NagleDemo-MessageReflector.vi" Type="VI" URL="../Nagle/NagleDemo-MessageReflector.vi"/>
				<Item Name="NagleDemo-MessageSender.vi" Type="VI" URL="../Nagle/NagleDemo-MessageSender.vi"/>
				<Item Name="TCP_NoDelay_Linux.vi" Type="VI" URL="../Nagle/TCP_NoDelay_Linux.vi"/>
				<Item Name="TCP_NoDelay_Phar_Lap.vi" Type="VI" URL="../Nagle/TCP_NoDelay_Phar_Lap.vi"/>
				<Item Name="TCP_NoDelay_VxWorks.vi" Type="VI" URL="../Nagle/TCP_NoDelay_VxWorks.vi"/>
				<Item Name="TCP_NoDelay_Windows.vi" Type="VI" URL="../Nagle/TCP_NoDelay_Windows.vi"/>
			</Item>
			<Item Name="EzQ" Type="Folder">
				<Item Name="EzQ message cluster.ctl" Type="VI" URL="../EzQ/Source/EzQ message cluster.ctl"/>
				<Item Name="EzQ Registration cluster.ctl" Type="VI" URL="../EzQ/Source/EzQ Registration cluster.ctl"/>
				<Item Name="Register EzQ.vi" Type="VI" URL="../EzQ/Source/Register EzQ.vi"/>
				<Item Name="Send to EzQ.vi" Type="VI" URL="../EzQ/Source/Send to EzQ.vi"/>
				<Item Name="EzQ Example.vi" Type="VI" URL="../EzQ/Source/EzQ Example.vi"/>
			</Item>
			<Item Name="Error Accumulator.lvclass" Type="LVClass" URL="../Error Accumulator/Error Accumulator.lvclass"/>
			<Item Name="Error trace.ctl" Type="VI" URL="../Error Accumulator/Error trace.ctl"/>
			<Item Name="Exitimer.lvclass" Type="LVClass" URL="../exitimer/Exitimer.lvclass"/>
			<Item Name="Log settings.ctl" Type="VI" URL="../Error Accumulator/Log settings.ctl"/>
			<Item Name="LogRotate.lvclass" Type="LVClass" URL="../LogRotate/LogRotate.lvclass"/>
		</Item>
		<Item Name="Typedefs" Type="Folder">
			<Item Name="Advanced Options Cluster.ctl" Type="VI" URL="../Source/Advanced Options Cluster.ctl"/>
			<Item Name="Message Cluster.ctl" Type="VI" URL="../Source/Message Cluster.ctl"/>
		</Item>
		<Item Name="EzTCP.lvclass" Type="LVClass" URL="../Source/EzTCP.lvclass"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="High Resolution Polling Wait.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Polling Wait.vi"/>
				<Item Name="TCP Get Raw Net Object.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Get Raw Net Object.vi"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="libsystem_kernel.dylib" Type="Document" URL="/usr/lib/system/libsystem_kernel.dylib"/>
			<Item Name="wsock32.dll" Type="Document" URL="wsock32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="libc.so.6" Type="Document" URL="libc.so.6">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
